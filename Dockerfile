FROM alpine:edge

RUN echo '@testing http://nl.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories
RUN apk update
RUN apk add --no-cache \
    xkeyboard-config \
    i3wm \
    i3status@testing \
    python \
    py-pip \
    py-psutil \
    python-dev \
    gcc

RUN pip install robotframework robotremoteserver

ENV DISPLAY=xvfb:0

COPY i3wm.sh /usr/local/bin/
COPY i3wmControl.py i3wmControl.py
COPY config /root/.i3/config

RUN chmod ug+x /usr/local/bin/i3wm.sh && \
    chmod ug+x i3wmControl.py
ENTRYPOINT ["/usr/local/bin/i3wm.sh"]
