from robot.api import logger
from robot.libraries.Process import Process

import time
import os

def remote_log_message(message, level, html=False):
    print '{} {}'.format(level, message)

logger.write = remote_log_message

class I3wmControlLibrary(object):

  def __init__(self):
    self._processlib = Process()
    self.i3wm = None

  def start_window_manager(self):
    if self.i3wm and self._processlib.is_process_running(self.i3wm):
      logger.write('{0} I3wm is already running'.format(time.time() * 1000), "INFO")
    else:
      logger.write('{0} Starting Up I3wm'.format(time.time() * 1000), "INFO")
      self.i3wm = self._processlib.start_process(
        "/usr/bin/i3", shell=True)

  def stop_window_manager(self):
    if self.i3wm and self._processlib.is_process_running(self.i3wm):
      logger.write("Killing I3wm", "INFO")
      self._processlib.terminate_process(self.i3wm)
      self.i3wm = None
    else:
      logger.write('{0} I3wm is not running'.format(time.time() * 1000), "INFO")

  def is_window_manager_running(self):
    logger.write('{0} Checking is I3wm running'.format(time.time() * 1000), "INFO")
    return self.i3wm and self._processlib.is_process_running(self.i3wm)

if __name__ == '__main__':
  import sys
  from robotremoteserver import RobotRemoteServer

  RobotRemoteServer(I3wmControlLibrary(), host="0.0.0.0", port=8270, allow_stop=False)
