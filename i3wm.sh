#!/bin/sh

case "$1" in
  remote)
    echo "Starting as Robotframework Remote Library"
    exec python i3wmControl.py
    ;;
  *)
    echo "Starting as Standalone"
    exec i3
    ;;
esac
